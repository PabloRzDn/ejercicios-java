package chileair;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GUI extends JFrame {
    
    private Container c;
    
    private JTextField nombres=new JTextField(20);
    private JTextField rut=new JTextField(10);
    private JTextField telefono=new JTextField(9);
    private JTextField direccion=new JTextField(100);
    private JComboBox region;
    private JComboBox mes;
    private JComboBox dia;
    private JComboBox hora;

    private JLabel titulo;
    private JLabel etiquetaNombre;
    private JLabel etiquetaRut;
    private JLabel etiquetaTelefono;
    private JLabel etiquetaDireccion;
    private JLabel etiquetaRegion;
    private JLabel etiquetaAtencion;
    private JLabel etiquetaHora;
    
    private JRadioButton atencionPrimaria;
    private JRadioButton hospitalizacion;
    private JRadioButton urgencias;

    private JButton enviar;

    private String regiones[]
        ={"Arica y parinacota",
            "Tarapacá",
            "Antofagasta",
            "Atacama",
            "Coquimbo",
            "Valparaíso",
            "Metropolitana",
            "Ohiggins",
            "Maule",
            "Ñuble",
            "Bío Bío",
            "Araucanía",
            "Los Ríos",
            "Los Lagos",
            "Aysen",
            "Magallanes"};


    private String meses[]
        ={
            "enero",
            "febrero",
            "marzo",
            "abril",
            "mayo",
            "junio",
            "julio",
            "agosto",
            "septiembre",
            "octubre",
            "noviembre",
            "diciembre"
        };

    private String dias[]
        ={"1","2","3","4","5","6","7",
          "8","9","10","11","12","13","14",
          "15","16","17","18","19","20","21",
          "22","23","24","25","26","27","28",
          "29","30","31"};

    private String horas[]
        ={"08:00","09:00","10:00","11:00","12:00",
          "13:00","14:00","15:00","16:00","17:00",
          "18:00","19:00","20:00"};

    public GUI(){
        setTitle("Centro Médico Azul");
        setSize(400,400);
        
        
        c=getContentPane();
        c.setLayout(null);  
            

        titulo=new JLabel("Para agendar su hora, ingrese sus datos personales");
        titulo.setFont(new Font("Arial", Font.PLAIN, 12));
        titulo.setSize(350, 15);
        titulo.setLocation(10, 10);
        c.add(titulo);
        
        etiquetaNombre=new JLabel("Nombre");
        etiquetaNombre.setFont(new Font("Arial", Font.PLAIN, 11));
        etiquetaNombre.setSize(350, 15);
        etiquetaNombre.setLocation(10, 30);
        c.add(etiquetaNombre);
        
        nombres= new JTextField(20);
        nombres.setFont(new Font("Arial", Font.PLAIN, 11));
        nombres.setSize(350,20);
        nombres.setLocation(10,50);
        c.add(nombres);

        etiquetaRut=new JLabel("Rut (sin puntos, con guión)");
        etiquetaRut.setFont(new Font("Arial", Font.PLAIN, 11));
        etiquetaRut.setSize(350, 15);
        etiquetaRut.setLocation(10, 70);
        c.add(etiquetaRut);
        
        rut= new JTextField(20);
        rut.setFont(new Font("Arial", Font.PLAIN, 11));
        rut.setSize(350,20);
        rut.setLocation(10,90);
        c.add(rut);

        etiquetaTelefono=new JLabel("Teléfono (+56)");
        etiquetaTelefono.setFont(new Font("Arial", Font.PLAIN, 11));
        etiquetaTelefono.setSize(350, 15);
        etiquetaTelefono.setLocation(10, 110);
        c.add(etiquetaTelefono);
        
        telefono= new JTextField(20);
        telefono.setFont(new Font("Arial", Font.PLAIN, 11));
        telefono.setSize(350,20);
        telefono.setLocation(10,130);
        c.add(telefono);

        etiquetaDireccion=new JLabel("Dirección");
        etiquetaDireccion.setFont(new Font("Arial", Font.PLAIN, 11));
        etiquetaDireccion.setSize(350, 15);
        etiquetaDireccion.setLocation(10, 150);
        c.add(etiquetaDireccion);
        
        direccion= new JTextField(20);
        direccion.setFont(new Font("Arial", Font.PLAIN, 11));
        direccion.setSize(350,20);
        direccion.setLocation(10,170);
        c.add(direccion);

        etiquetaRegion=new JLabel("Región");
        etiquetaRegion.setFont(new Font("Arial", Font.PLAIN, 11));
        etiquetaRegion.setSize(350, 15);
        etiquetaRegion.setLocation(10, 190);
        c.add(etiquetaRegion);
        
        
        region= new JComboBox(regiones);
        region.setFont(new Font("Arial",Font.PLAIN,11));
        region.setSize(350,20);
        region.setLocation(10,210);
        region.setSelectedIndex(0);
        c.add(region);



        etiquetaAtencion=new JLabel("Atención");
        etiquetaAtencion.setFont(new Font("Arial", Font.PLAIN, 11));
        etiquetaAtencion.setSize(350, 15);
        etiquetaAtencion.setLocation(10, 240);
        c.add(etiquetaAtencion);


        atencionPrimaria= new JRadioButton("Atención Primaria");
        atencionPrimaria.setFont(new Font("Arial",Font.PLAIN,11));
        atencionPrimaria.setSelected(true);
        atencionPrimaria.setSize(140,20);
        atencionPrimaria.setLocation(5,260);
        c.add(atencionPrimaria);

        hospitalizacion= new JRadioButton("Hospitalización");
        hospitalizacion.setFont(new Font("Arial",Font.PLAIN,11));
        hospitalizacion.setSelected(false);
        hospitalizacion.setSize(110,20);
        hospitalizacion.setLocation(145,260);
        c.add(hospitalizacion);

        
        urgencias= new JRadioButton("Urgencia");
        urgencias.setFont(new Font("Arial",Font.PLAIN,11));
        urgencias.setSelected(false);
        urgencias.setSize(100,20);
        urgencias.setLocation(280,260);
        c.add(urgencias);


        etiquetaHora=new JLabel("Hora");
        etiquetaHora.setFont(new Font("Arial", Font.PLAIN, 11));
        etiquetaHora.setSize(350, 15);
        etiquetaHora.setLocation(10, 280);
        c.add(etiquetaHora);

        mes= new JComboBox(meses);
        mes.setFont(new Font("Arial",Font.PLAIN,11));
        mes.setSize(80,20);
        mes.setLocation(10,300);
        mes.setSelectedIndex(0);
        c.add(mes);

        dia= new JComboBox(dias);
        dia.setFont(new Font("Arial",Font.PLAIN,11));
        dia.setSize(80,20);
        dia.setLocation(130,300);
        dia.setSelectedIndex(0);
        c.add(dia);

        hora= new JComboBox(horas);
        hora.setFont(new Font("Arial",Font.PLAIN,11));
        hora.setSize(80,20);
        hora.setLocation(260,300);
        hora.setSelectedIndex(0);
        c.add(hora);

        enviar= new JButton("Enviar");
        enviar.setFont(new Font ("Arial",Font.PLAIN,11));
        enviar.setSize(100,20);
        enviar.setLocation(130,330);
        c.add(enviar);        

        setVisible(true);
        
    }

    public void accionRealizada(ActionEvent e){
        if (e.getSource()==enviar){

        }

    }
    
  

    public static void main(String[] args){
        JFrame gui=new GUI();
        
    }
}
