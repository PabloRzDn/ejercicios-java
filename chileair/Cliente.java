

package chileair;
import java.util.*;

public class Cliente{
    private String rut;
    private String nombre;
    private String apellido;
    private int edad;
    private Date FechaVuelo;
    private int reservas=0;
    private double costo=0.0;
    private int cantidadPasajes=0;
    
    
    public Cliente(String rut, String nombre, String apellido, int edad, Date FechaVuelo, double costo){
        this.rut=rut;
        this.nombre=nombre;
        this.apellido=apellido;
        this.edad=edad;
        this.FechaVuelo=FechaVuelo;
        this.costo=costo;
    }
 
    public double calcularDescuentos(){
           
        if(edad<8){
            costo=costo*0.8;
            
        } else if(costo>65){
            costo=costo*0.7;
           
        }
        
        return costo;

    }
    public List<Object> ConfirmarPasaje(){
        List<Object> pasajero = new ArrayList<Object>();
        pasajero.add(rut);
        pasajero.add(nombre);
        pasajero.add(apellido);    
        pasajero.add(edad);
        pasajero.add(FechaVuelo);
        
        return pasajero;        
    }
    public int SumarPasaje(){
        cantidadPasajes+=cantidadPasajes;
        return cantidadPasajes;
    }

   

    
    
    
    

} 


