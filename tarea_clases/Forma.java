package tarea_clases;
public class Forma
{
     private class Centro{

        private double x,y;
        public String setCentro(double newX, double newY)
        {
            this.x=newX;
            this.y=newY;

            return String.format("(%f,%f)",this.x,this.y);
        }
    

    }

    private char nombre_forma;
    private String color;
    private String centro;
    
    public char getNombreForma(){
        return nombre_forma;
    }
    public String getColor(){
        return color;
    }
    public String getCentro(){
        return centro;
    }  
    public void setNombreForma(char newForma){
        this.nombre_forma=newForma;
    }

    public void setColor(String newColor){
        this.color=newColor;
    }

    public void setCentroForma(double x, double y)
    {
        Centro center=new Centro();
        this.centro=center.setCentro(x,y);
    }
    public String Imprimir()
    {
        return String.format("Forma: %s, Color %s, Centro %s",nombre_forma,color,centro);
    }

   
}


