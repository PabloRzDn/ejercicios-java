package tarea_clases;

public class Ejecutar {
    public static void main(String[] args){
        
        Forma cuadrado= new Forma();
        cuadrado.setNombreForma('c');
        cuadrado.setColor("rojo");
        cuadrado.setCentroForma(2, 1);
        
        System.out.println(cuadrado.Imprimir());
        
        cuadrado.setColor("verde");
        System.out.println(cuadrado.Imprimir());
        
        Rectangulo rect=new Rectangulo();
        rect.setLados(2, 3);
        System.out.println(String.format("Lado Mayor: %s, Lado Menor %s",rect.getLados()[0],rect.getLados()[1]));
        System.out.println(String.format("Perímetro: %s unid., Área: %s unid2",rect.perimetro(),rect.area()));
        
        System.out.println(String.format("Factor: %s, Nuevo l. mayor: %s, nuevo l. menor: %s, nueva área: %s, nuevo perímetro: %s",rect.cambiarTamano(0.5)[0],rect.cambiarTamano(0.5)[1],rect.cambiarTamano(0.5)[2],rect.cambiarTamano(0.5)[3],rect.cambiarTamano(0.5)[4]));
    }
    
}
