package tarea_clases;
public class Rectangulo extends Forma {
    private double lado_menor, lado_mayor;
    private double area_rect;
    
    private double perimetro_rect;

    public void setLados(double newLadoMenor, double newLadoMayor){
        this.lado_menor=newLadoMenor;
        this.lado_mayor=newLadoMayor;
    }

    public double[] getLados(){
        double[] lados= new double[2];
        lados[0]=lado_mayor;
        lados[1]=lado_menor;
        return lados;
    }

    public double area(){

        area_rect=lado_mayor*lado_menor;
        return area_rect;

    }
    public double perimetro(){
        perimetro_rect=(2*lado_mayor+2*lado_menor);
        return perimetro_rect;
    }

    public double[] cambiarTamano(double factor){
        lado_mayor=factor*lado_mayor;
        lado_menor=factor*lado_menor;
        setLados(lado_menor, lado_mayor);
        area_rect=area();
        perimetro_rect=perimetro();
        
        double[] nuevoRectangulo=new double[5];
        nuevoRectangulo[0]=factor;
        nuevoRectangulo[1]=lado_mayor;
        nuevoRectangulo[2]=lado_menor;
        nuevoRectangulo[3]=area_rect;
        nuevoRectangulo[4]=perimetro_rect;
        
        return nuevoRectangulo;
    }

    
    
}
