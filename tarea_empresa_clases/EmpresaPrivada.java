package tarea_empresa_clases;

public class EmpresaPrivada extends Empresa{
    
    String accionistaMayoritario;
    Boolean impuestosPagadosAFecha;
    
    public EmpresaPrivada(String nombreEmpresa,String rutEmpresa, String contacto, String direccion){
        super(nombreEmpresa,rutEmpresa,contacto,direccion);
    }
    public void setAccionistaMayoritario(String accionistaMayoritario){
        this.accionistaMayoritario=accionistaMayoritario;
    }

    public String getAccionistaMayoritario(){
        return accionistaMayoritario;
    }

    public void setImpuestosPagadosAlaFecha(boolean impuestosPagadosAFecha){
        this.impuestosPagadosAFecha=impuestosPagadosAFecha;
    }
    public boolean getImpuestosPagadosAlaFecha(){
        return impuestosPagadosAFecha;
    }
}
