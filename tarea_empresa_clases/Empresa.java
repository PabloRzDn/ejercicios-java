package tarea_empresa_clases;

public class Empresa {

    private String nombreEmpresa;
    private String rutEmpresa;
    private String contacto;
    private String direccion;
    
    private double ventas;
    private double compras;
    private double balance;
    
    public Empresa(String nombreEmpresa, String rutEmpresa, String contacto, String direccion){
      this.nombreEmpresa=nombreEmpresa;
      this.rutEmpresa=rutEmpresa;
      this.contacto=contacto;
      this.direccion=direccion;
    }
    public String imprimir(){
        return String.format("Empresa: %s, rut: %s, contacto: %s, direccion:%s", nombreEmpresa,rutEmpresa,contacto,direccion);
    }
    public void setEmpresa(String nombre){
        nombreEmpresa=nombre;
    }
    public void setRut(String rut){
        rutEmpresa=rut;
    }
    public void setContacto(String cont){
        contacto=cont;
    }
    public void seDireccion(String dir){
        direccion=dir;
    }
    public String getEmpresa(){
        return nombreEmpresa;
    }
    public String getRut(){
        return rutEmpresa;
    }
    public String getContacto(){
        return contacto;
    }
    public String getDireccion(){
        return direccion;
    }

    public void setVentas(double ventas){
        this.ventas=ventas;
    }

    public double getVentas(){
        return ventas;
    }

    public double realizarVentas(double ventaRealizada){
        ventas=ventas+ventaRealizada;
        return ventas;
    }

    public void setCompras(double compras){
        this.compras=compras;
    }

    public double getCompras(){
        return compras;
    }

    public double realizarCompras(double compraRealizada){
        compras=compras+compraRealizada;
        return compras;
    }

    public double getBalance(){
        balance=(ventas-compras);
        return balance;
    }
    


    
}

