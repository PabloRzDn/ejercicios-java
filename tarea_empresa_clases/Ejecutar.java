package tarea_empresa_clases;

public class Ejecutar {

    public static void main(String[] args){
        //Se crea objeto EmpresaPublica, que hereda de la clase Empresa
        EmpresaPublica codelco=new EmpresaPublica("codelco", "1111111-1", "12345679", "Alameda 666");
        System.out.println(codelco.imprimir());
        //Se emplean los métodos de la subclase EmpresaPublica.
        codelco.setArea("Extracción minera");
        codelco.setDependencia("Ministerio de Minería");
        String area=codelco.getArea();
        String dependencia=codelco.getDependencia();

        System.out.println(String.format("área: %s, dependencia: %s",area,dependencia));

        //se utiliza el método realizarVentas, que retorna un double
        double venta1=codelco.realizarVentas(3000);
        System.out.println(String.format("Se ha realizado una venta por: $%s",venta1));
        //se utiliza el método realizarCompras, que retorna un double
        double venta2=codelco.realizarVentas(2400);
        System.out.println(String.format("Se ha realizado una compra por: $%s",venta2));

        double compra1=codelco.realizarCompras(1000);
        System.out.println(String.format("Se ha realizado una compra por: $%s",compra1));
        double compra2=codelco.realizarCompras(1000);
        System.out.println(String.format("Se ha realizado una compra por: $%s",compra2));

        //se utiliza el método getBalance, que retorna un double
        double balance=codelco.getBalance();
        System.out.println(String.format("El balance es: $%s",balance));

        //Se crea objeto EmpresaPrivada, que hereda de la clase Empresa
        EmpresaPrivada adidas= new EmpresaPrivada("Adidas", "2222222-2", "987654321", "Panamericana norte");
        System.out.println(adidas.imprimir());

        //Se emplean métodos de la subclase EmpresaPrivada
        adidas.setAccionistaMayoritario("BBB ALO Fund.");
        adidas.setImpuestosPagadosAlaFecha(true);

        String accionista_mayoritario=adidas.getAccionistaMayoritario();
        boolean impuestos=adidas.getImpuestosPagadosAlaFecha();

        System.out.println(String.format("Accionista mayoritario:%s, ¿impuestos pagados?: %s",accionista_mayoritario,impuestos));

        //se utiliza el método realizarVentas, que retorna un double
        double venta3=adidas.realizarVentas(5000);
        System.out.println(String.format("Se ha realizado una venta por: $%s",venta3));
        //se utiliza el método realizarCompras, que retorna un double
        double venta4=adidas.realizarVentas(4000);
        System.out.println(String.format("Se ha realizado una compra por: $%s",venta4));

        double compra3=adidas.realizarCompras(2000);
        System.out.println(String.format("Se ha realizado una compra por: $%s",compra3));
        double compra4=adidas.realizarCompras(2000);
        System.out.println(String.format("Se ha realizado una compra por: $%s",compra4));

        //se utiliza el método getBalance, que retorna un double
        double balanceprivado=adidas.getBalance();
        System.out.println(String.format("El balance es: $%s",balanceprivado));
        
    }
    
}
