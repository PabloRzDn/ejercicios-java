package tarea_empresa_clases;

public class EmpresaPublica extends Empresa {

    private String area;
    private String dependencia;
    

    public EmpresaPublica(String nombreEmpresa,String rutEmpresa, String contacto, String direccion){
        super(nombreEmpresa,rutEmpresa,contacto,direccion);
        
    }

    public void setArea(String area){
        this.area=area;
    }

    public String getArea(){
        return area;
    }

    public void setDependencia(String dependencia){
        this.dependencia=dependencia;
    }

    public String getDependencia(){
        return dependencia;
    }


    
}
