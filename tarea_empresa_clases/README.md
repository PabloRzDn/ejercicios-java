1. Suponga que existe una clase denominada empresa y se requieren declarar las clases:
empresa pública y empresa privada
a. Realice el diagrama gráfico de la clase y las subclases o clases derivadas.
b. Defina métodos y atributos.
c. Explique el objetivo de usar la herencia en este caso.
d. Realice las líneas de código que definan en Java estas clases donde se utilice la
herencia.
2. Cree una nueva clase denomina empresa mixta que sea capaz de heredar algunas
propiedades de las empresas públicas, y otras de las empresas privadas, seleccione usted
métodos y atributos que considere se deben heredar. Presente la representación gráfica. 